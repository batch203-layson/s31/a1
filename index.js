/*

9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.
11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.

*/

const http = require("http");

const port = 3000;

const server = http.createServer((req, res) => {
    if (req.url == "/login") {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("Welcome to the login page");
    } else {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end("I am sorry the page you are looking for cannot be found.");
    }
});

server.listen(port);

console.log('Server is now accessible at localhost:${port}');